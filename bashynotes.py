#!/bin/python

import json, os, re, sys
osp = os.path

minWidth=24

YELLOW='\033[1;33m'
NC='\033[0m' # No Color

def getTTYSize():
    """Get the current TTY size"""
    rows, columns = os.popen('stty size', 'r').read().split()
    return rows, columns

class NoteManager:
    def __init__(self, filename = '.notes.txt'):
        self.filename = osp.join(osp.dirname(osp.realpath(__file__)), filename)
        self.notes = self.loadNotes()
        self.__written_chars = 0

    def __del__(self):
        self.saveNotes()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.saveNotes()

    def loadNotes(self):
        """Load the JSON notes from disk"""
        try:
            with open(self.filename) as f:
                return json.loads(f.read())
        except (FileNotFoundError, json.decoder.JSONDecodeError):
            return []

    def saveNotes(self):
        """Save the notes to disk as JSON"""
        try:
            with open(self.filename, 'w') as f:
                f.write(json.dumps(self.notes))
        except FileNotFoundError:
            return False

        return True

    def addNote(self, note):
        """Add a note to memory"""
        try:
            self.notes.append(note)
        except:
            return False

        return True

    def deleteNote(self, index):
        """Delete note from memory"""
        try:
            del self.notes[index]
        except KeyError:
            return False

        return True

    def printHeadFoot(self):
        """Print header/footer"""
        rows, columns = getTTYSize()
        for x in range(0, int(columns)):
            if x == 0:
                sys.stdout.write(YELLOW+'+'+NC)
            elif x == int(columns)-1:
                sys.stdout.write(YELLOW+'+'+NC+'\n')
            else:
                sys.stdout.write(YELLOW+'-'+NC)

    def startLine(self):
        sys.stdout.write(YELLOW+'| '+NC)
        self.__written_chars = 2

    def endLine(self, newline=True):
        sys.stdout.write(YELLOW+'|'+NC)
        if newline:
            sys.stdout.write('\n')
        self.__written_chars = 0

    def fillEmptySpace(self):
        rows, columns = getTTYSize()

        while self.__written_chars < int(columns)-1:
            sys.stdout.write(' ')
            self.__written_chars += 1

    def write(self, char):
        if len(char) != 1:
            raise ValueError

        if not (self.__written_chars == 2 and re.search('\s', char)):
            sys.stdout.write(YELLOW+char+NC)
            self.__written_chars += 1

    def printNote(self, note):
        """Print a note (with proper formatting)"""
        rows, columns = getTTYSize()

        self.printHeadFoot()

        self.startLine()

        note_list = list(note)
        for i, char in enumerate(note_list):
            if char == '\n':
                self.fillEmptySpace()
                self.endLine(newline=False)
                self.write(char)
                self.startLine()
                continue

            self.write(char)

            # if we reached the end, break up the line
            if self.__written_chars == int(columns)-2:
                if i == len(note_list)-1: # last character of note
                    sys.stdout.write(' ')
                    # do some special handling here
                elif (re.search('\s', char) or
                      (len(note_list) > i+1 and
                       re.search('\s', note_list[i+1]))):
                    sys.stdout.write(' ')
                else:
                    sys.stdout.write('-')

                self.endLine()
                self.startLine()

        self.fillEmptySpace()
        self.endLine()

        self.printHeadFoot()

    def printNotes(self):
        """Print all notes"""
        for note in self.notes:
            self.printNote(note)

def main():
    notemgr = NoteManager()
    if len(sys.argv) <= 1:
        notemgr.printNotes()
        sys.exit(0)

    action = sys.argv[1]

    if action == 'add':
        try:
            notemgr.addNote(sys.argv[2])
        except IndexError:
            print('Missing parameter')
    elif action == 'delete':
        try:
            notemgr.deleteNote(sys.argv[2])
        except IndexError:
            print('Missing parameter')
    else:
        notemgr.printNotes()

if __name__ == '__main__':
    main()
